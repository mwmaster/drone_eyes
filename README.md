"gscam_rgba8_support.cpp"
-edited /gscam/src/gscam.cpp to support rgba img format to resolve compatibility issue
-replace gscam.cpp in gscam/src dir with this file if rebuilding 

"record_data.sh"
-start rosbag in current directory
*TODO: bag to target location

"test.launch"
-working launch file for 4/22/2022 flight camera suite
*TODO: auto calibrate

If gstreamer throws error: "... Failed to create CaptureSession," fix by restarting nvargus-daemon
